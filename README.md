# Chronometer, an android application

This is a simple application used to demostrate several design patterns in action. These are some of design patterns used in the project:

* Model-View-Presenter architecture with very humble views
* Unit tests using mockito
* GUI with fragments

## Features

* A single chronometter
* A multi-round chronometter
* An alarm clock which counts down from a specified time to zero

## Screenshots

![Simple counter](https://framagit.org/babakRz63ai/chronometter-android-app/blob/master/screenshots/Screenshot-page-1.png)

![Counter with several rounds](https://framagit.org/babakRz63ai/chronometter-android-app/blob/master/screenshots/Screenshot-page-2.png)

![Alarm clock](https://framagit.org/babakRz63ai/chronometter-android-app/blob/master/screenshots/Screenshot-page-3.png)



