package geek.bobby.chronometer;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import geek.bobby.chronometer.timing.SubHourTime;

import static org.junit.Assert.assertEquals;

/**
 * Tests SubHourTime model
 *
 */
public class TestSubHourTime {
    @Test
    public void readAndWrite() {
        SubHourTime subHourTime = new SubHourTime();
        subHourTime.setMinute(15);
        subHourTime.setSecond(28);
        subHourTime.setCentisecond(89);
        assertEquals(15, subHourTime.getMinute());
        assertEquals(28, subHourTime.getSecond());
        assertEquals(89, subHourTime.getCentisecond());
    }

    @Test
    public void test_zero()
    {
        SubHourTime subHourTime = new SubHourTime();
        subHourTime.zero();
        assertEquals(0, subHourTime.getMinute());
        assertEquals(0, subHourTime.getSecond());
        assertEquals(0, subHourTime.getCentisecond());
    }

    //test increment
    @Test
    public void testIncrement()
    {
        SubHourTime time = new SubHourTime();
        time.setMinute(15);
        time.setSecond(28);
        time.setCentisecond(89);
        time.increment(2);
        assertEquals(91, time.getCentisecond());
        assertEquals(28, time.getSecond());
        assertEquals(15, time.getMinute());

        time.increment(9);
        assertEquals(0, time.getCentisecond());
        assertEquals(29, time.getSecond());
        assertEquals(15, time.getMinute());

        time.increment(113);
        assertEquals(13, time.getCentisecond());
        assertEquals(30, time.getSecond());
        assertEquals(15, time.getMinute());

        time.zero();
        time.setCentisecond(99);
        time.setSecond(59);
        time.increment(1);
        assertEquals(0, time.getCentisecond());
        assertEquals(0, time.getSecond());
        assertEquals(1, time.getMinute());
    }

    @Test
    public void testIncrementMilli()
    {
        SubHourTime time = new SubHourTime();
        time.setMinute(15);
        time.setSecond(28);
        time.setCentisecond(89);
        time.incrementMillis(24);
        assertEquals(91, time.getCentisecond());
        assertEquals(28, time.getSecond());
        assertEquals(15, time.getMinute());

        time.incrementMillis(86);
        assertEquals(0, time.getCentisecond());
        assertEquals(29, time.getSecond());
        assertEquals(15, time.getMinute());

        time.zero();
        time.incrementMillis(1994);
        assertEquals(99, time.getCentisecond());
        assertEquals(1, time.getSecond());
        assertEquals(0, time.getMinute());

        time.zero();
        time.incrementMillis(1995);
        assertEquals(0, time.getCentisecond());
        assertEquals(2, time.getSecond());
        assertEquals(0, time.getMinute());

        time.zero();
        time.incrementMillis(59995);
        assertEquals(0, time.getCentisecond());
        assertEquals(0, time.getSecond());
        assertEquals(1, time.getMinute());
    }

    @Test
    public void isSerializable() throws IOException, ClassNotFoundException
    {
        SubHourTime time = new SubHourTime();
        time.setMinute(12);
        time.setSecond(13);
        time.setCentisecond(56);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(time);
        objectOutputStream.flush();
        objectOutputStream.close();

        ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        SubHourTime time2 = (SubHourTime)inputStream.readObject();
        assertEquals(time2, time);
    }
}