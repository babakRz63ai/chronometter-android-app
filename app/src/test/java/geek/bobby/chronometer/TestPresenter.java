package geek.bobby.chronometer;

import org.junit.Test;

import geek.bobby.chronometer.timing.ITimingContract;
import geek.bobby.chronometer.timing.SimpleTimingPresenter;
import geek.bobby.chronometer.timing.SubHourTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


public class TestPresenter {
    @Test
    public void initial_value()
    {
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        SubHourTime time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(0, time.getSecond());
        assertEquals(0, time.getCentisecond());
    }

    @Test
    public void sets_view_on_each_pulse() {
        //create a presenter and a mock view; give presenter the view; pulse the presenter;
        // assert view.displayTime was called by correct time values
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        ITimingContract.ICounterView counterView = mock(ITimingContract.ICounterView.class);
        presenter.onPulse();
        presenter.setView(counterView);
        verify(counterView).displayTime(any(SubHourTime.class));

        presenter.onPulse();
        verify(counterView).displayTime(any(SubHourTime.class));
    }

    @Test
    public void displays_zero_on_reset()
    {
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        ITimingContract.ICounterView counterView = mock(ITimingContract.ICounterView.class);
        presenter.setView(counterView);
        verify(counterView).displayTime(new SubHourTime());
    }

    @Test
    public void several_pulse_durations() throws InterruptedException
    {
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        presenter.setView(mock(ITimingContract.ICounterView.class));
        presenter.onPulse();
        Thread.sleep(9);
        presenter.onPulse();
        SubHourTime time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(0, time.getSecond());
        assertEquals(1, time.getCentisecond());
        Thread.sleep(100);
        presenter.onPulse();
        time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(0, time.getSecond());
        assertEquals(11, time.getCentisecond());

        presenter.reset();
        presenter.onPulse();
        Thread.sleep(2000); // two seconds
        presenter.onPulse();
        time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(2, time.getSecond());
        assertEquals(0, time.getCentisecond());

        presenter.reset();
        presenter.onPulse();
        Thread.sleep(12345); // 12.35 seconds
        presenter.onPulse();
        time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(12, time.getSecond());
        assertEquals(35, time.getCentisecond());
    }

    @Test
    public void several_countdown_pulse_durations() throws InterruptedException
    {
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        presenter.setView(mock(ITimingContract.ICounterView.class));
        SubHourTime initial = new SubHourTime();
        initial.setMinute(0);
        initial.setSecond(0);
        initial.setCentisecond(1);
        presenter.setCurrentTimerValue(initial);
        presenter.onPulse();
        Thread.sleep(9);
        presenter.onCountDownPulse();
        SubHourTime time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(0, time.getSecond());
        assertEquals(0, time.getCentisecond());

        initial.setMinute(0);
        initial.setSecond(10);
        initial.setCentisecond(0);
        Thread.sleep(1000);
        presenter.onCountDownPulse();
        time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(9, time.getSecond());
        assertEquals(0, time.getCentisecond());

        initial.setMinute(1);
        initial.setSecond(0);
        initial.setCentisecond(0);
        Thread.sleep(1000);
        presenter.onCountDownPulse();
        time = presenter.getCurrentTimerValue();
        assertEquals(0, time.getMinute());
        assertEquals(59, time.getSecond());
        assertEquals(0, time.getCentisecond());
    }



    @Test
    public void calls_finish_listener() throws InterruptedException
    {
        // We use a mock Runnable
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        Runnable mockListener = mock(Runnable.class);
        presenter.setOnFinishListener(mockListener);
        SubHourTime subHourTime = new SubHourTime();
        subHourTime.setMinute(59);
        subHourTime.setSecond(59);
        subHourTime.setCentisecond(98);
        presenter.setCurrentTimerValue(subHourTime);
        presenter.onPulse();
        Thread.sleep(20);
        presenter.onPulse();
        verify(mockListener).run();
    }

    @Test
    public void calls_finish_listener_after_countdown() throws InterruptedException
    {
        SimpleTimingPresenter presenter = new SimpleTimingPresenter();
        Runnable mockListener = mock(Runnable.class);
        presenter.setOnFinishListener(mockListener);
        SubHourTime subHourTime = new SubHourTime();
        subHourTime.setMinute(0);
        subHourTime.setSecond(0);
        subHourTime.setCentisecond(3);
        presenter.setCurrentTimerValue(subHourTime);
        presenter.onCountDownPulse();
        Thread.sleep(10);
        presenter.onCountDownPulse();
        Thread.sleep(10);
        presenter.onCountDownPulse();
        Thread.sleep(11);
        presenter.onCountDownPulse();
        verify(mockListener).run();
    }


}