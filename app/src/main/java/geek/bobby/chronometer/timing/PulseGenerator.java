package geek.bobby.chronometer.timing;

import android.os.Handler;
import android.support.annotation.NonNull;


/**
 * Takes a presenter and pulses it in regular time intervals
 */
public class PulseGenerator implements Runnable {

    public enum Direction
    {
        CountUp,
        CountDown
    }

    private final int incrementMilliseconds;
    private final ITimingContract.ITimePresenter presenter;
    private final Handler handler;
    private boolean isRunning;
    private Direction direction = Direction.CountUp;

    public PulseGenerator(@NonNull ITimingContract.ITimePresenter aPresenter,
                          int increment100thSecs)
    {
        incrementMilliseconds = 10*increment100thSecs;
        presenter = aPresenter;
        handler = new Handler();
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public void run() {
// Enables an accurate timing according to expected delay between ticks and time taken to
        // update models and rendering output
        long tick = System.currentTimeMillis();

        if (direction==Direction.CountUp)
            presenter.onPulse();
        else
            presenter.onCountDownPulse();

        if (isRunning)
            handler.postDelayed(this,
                    Math.max(0,incrementMilliseconds-(System.currentTimeMillis()-tick)));
    }

    public void start()
    {
        isRunning = true;
        handler.postDelayed(this, incrementMilliseconds);
    }

    public void stop()
    {
        isRunning = false;
        handler.removeCallbacks(this);
    }

    public boolean isRunning() {
        return isRunning;
    }
}
