package geek.bobby.chronometer.timing;

import java.io.Serializable;

import geek.bobby.chronometer.utils.IntegrityCheck;

public class SubHourTime implements Serializable
{
    private int minute,     // in range [0,59]
            second,         // in range [0,59]
            millisecond;    // in range [0,999]

    public int getCentisecond() {
        int c = millisecond/10;
        if (millisecond%10 >= 5)
            c++;
        if (c==100)
            return 0;
        else
            return c;
    }

    public int getMinute() {
        if (millisecond>=995 && second==59)
            return minute+1;
        else
            return minute;
    }

    public int getSecond() {
        if (millisecond>=995)
            return (second+1)%60;
        else
            return second;
    }

    public void setCentisecond(int centisecond) {
        IntegrityCheck.checkCondition(0<=centisecond && centisecond<100,
                "centisecond must be between 0 and 99");
        this.millisecond = centisecond*10;
    }

    public void setMinute(int minute) {
        IntegrityCheck.checkCondition(0<=minute && minute<60,
                "Minutes must be between 0 and 59");
        this.minute = minute;
    }

    public void setSecond(int second) {
        IntegrityCheck.checkCondition(0<=second && second<60,
                "Minutes must be between 0 and 59");
        this.second = second;
    }

    public void zero()
    {
        minute = 0;
        second = 0;
        millisecond = 0;
    }

    public boolean increment(int centiseconds)
    {
        return incrementMillis(10*centiseconds);
    }

    /**
     *     Increment as much as one tick
     * @return true if the time value was small enough so an increment has been possible
     */
    public boolean incrementMillis(long ms)
    {
        millisecond += ms;
        if (millisecond>=1000)
        {
            second += (millisecond/1000);
            millisecond %= 1000;

            if (second>=60)
            {
                minute += second/60;
                second %= 60;
            }
        }

        return minute<60;
    }

    /**
     * Decrement its current value as much as a given amount of milliseconds
     * @param ms amount of time in milliseconds
     * @return true if the time value was big enough to stay positive after decrease
     */
    public boolean decrementMillis(long ms)
    {
        millisecond -= ms;
        if (millisecond<0)
        {
            second += (millisecond/1000 - 1);
            millisecond = 1000+(millisecond%1000); // -1234 -> 1000-234 = 766
            if (second<0)
            {
                minute += (second/60 - 1);
                second = 60+(second%60); // 2:-4 => 1:56
            }
        }

        return minute>=0;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof SubHourTime)
            return ((SubHourTime) other).millisecond == this.millisecond &&
                ((SubHourTime) other).second == this.second &&
                ((SubHourTime) other).minute == this.minute;
        else
            return super.equals(other);
    }

    @Override
    protected final Object clone()
    {
        SubHourTime clone = new SubHourTime();
        clone.minute = this.minute;
        clone.second = this.second;
        clone.millisecond = this.millisecond;
        return clone;
    }
}
