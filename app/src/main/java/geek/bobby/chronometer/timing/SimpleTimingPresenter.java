package geek.bobby.chronometer.timing;

import android.support.annotation.NonNull;

public class SimpleTimingPresenter implements ITimingContract.ITimePresenter {
    private ITimingContract.ICounterView myView;
    private SubHourTime subHourTime = new SubHourTime();
    private Runnable finishListener;
    private long lastPulse=-1;

    @Override
    public void setView(@NonNull ITimingContract.ICounterView view) {
        myView = view;
        myView.displayTime(subHourTime);
    }

    @Override
    public void reset() {
        subHourTime.zero();
        lastPulse=-1;
        myView.displayTime(subHourTime);
    }

    @Override
    public void onPulse() {
        long current = System.currentTimeMillis();

        if (commonPulseHandling(current))
            return;

        if (subHourTime.incrementMillis(current-lastPulse)) {
            if (myView!=null)
                myView.displayTime(subHourTime);
        }
        else if (finishListener != null)
            finishListener.run();

        lastPulse = current;
    }

    @Override
    public void onCountDownPulse() {
        long current = System.currentTimeMillis();

        if (commonPulseHandling(current))
            return;

        if (subHourTime.decrementMillis(current-lastPulse)) {
            if (myView!=null)
                myView.displayTime(subHourTime);
        }
        else {
            subHourTime.zero();
            myView.displayTime(subHourTime);
            if (finishListener != null)
                finishListener.run();
        }

        lastPulse = current;
    }

    private boolean commonPulseHandling(long current)
    {
        if (subHourTime==null)
            // This can happen after onDestroy() is called
            return true;

        if (lastPulse==-1) {
            lastPulse = current;
            if (myView!=null)
                myView.displayTime(subHourTime);
            return true;
        }

        return false;
    }

    @NonNull
    @Override
    public SubHourTime getCurrentTimerValue() {
        return (SubHourTime)subHourTime.clone();
    }

    @Override
    public void setOnFinishListener(@NonNull Runnable runnable) {
        finishListener = runnable;
    }

    @Override
    public void onDestroy() {
        subHourTime=null;
        finishListener=null;
    }

    @Override
    public void setCurrentTimerValue(@NonNull SubHourTime value) {
        subHourTime = value;
        if (myView!=null)
            myView.displayTime(subHourTime);
    }
}
