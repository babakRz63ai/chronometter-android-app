package geek.bobby.chronometer.utils;

import android.support.annotation.NonNull;

import geek.bobby.chronometer.BuildConfig;

public class IntegrityCheck {
    public static void checkCondition(boolean condition, @NonNull String messageIfFalse)
    {
        if (!condition && BuildConfig.DEBUG)
            throw new IllegalStateException(messageIfFalse);
    }
}
