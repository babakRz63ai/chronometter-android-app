package geek.bobby.chronometer.ui.main;

import android.animation.ValueAnimator;
import android.content.ContentResolver;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.io.IOException;

import geek.bobby.chronometer.R;
import geek.bobby.chronometer.timing.SubHourTime;

public class FrgReverseCounter extends BaseCounterFragment
    implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener
{
    private static final String TAG = "FrgReverseCounter";

    private NumberPicker minutesPicker, secondsPicker, centiSecondsPicker;
    private CounterView counterView;
    private ValueAnimator redGradient;
    private TextView btnStart;
    private MediaPlayer mediaPlayer;
    private boolean initializedMediaPlayer = false;


    public FrgReverseCounter() {

        // An animator for a color changing effect on the counter
        redGradient = ValueAnimator.ofInt(0,255);
        redGradient.setRepeatCount(ValueAnimator.INFINITE);
        redGradient.setRepeatMode(ValueAnimator.REVERSE);
        redGradient.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                counterView.setTextColor(Color.argb(138, // 54% of 255
                        (int)valueAnimator.getAnimatedValue(),
                        0,0));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // A media player to play the alarm sound
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        initializeMediaPlayer();
    }

    @Override
    public void onStop() {
        if (mediaPlayer!=null)
        {
            mediaPlayer.release();
            mediaPlayer = null;
        }

        initializedMediaPlayer = false;

        super.onStop();
    }

    private void initializeMediaPlayer()
    {
        mediaPlayer.reset();
        try {
            if (Build.VERSION.SDK_INT>=24)
                mediaPlayer.setDataSource(getResources().openRawResourceFd(R.raw.clock_beep));
            else {
                final Uri uri = new Uri.Builder()
                        .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                        .authority("geek.bobby.chronometer")
                        .appendPath(getResources().getResourceTypeName(R.raw.clock_beep))
                        .appendPath(getResources().getResourceEntryName(R.raw.clock_beep))
                        .build();
                mediaPlayer.setDataSource(getContext(), uri);
            }

            initializedMediaPlayer = true;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (IllegalArgumentException ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        Log.e(TAG,"MediaPlayer error "+what+" : "+extra);
        initializedMediaPlayer = false;
        initializeMediaPlayer();
        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_reverse_counter, container,
                false);

        minutesPicker = view.findViewById(R.id.minutePicker);
        secondsPicker = view.findViewById(R.id.secondsPicker);
        centiSecondsPicker = view.findViewById(R.id.centiPicker);

        minutesPicker.setMinValue(0);
        minutesPicker.setMaxValue(59);
        secondsPicker.setMinValue(0);
        secondsPicker.setMaxValue(59);
        centiSecondsPicker.setMinValue(0);
        centiSecondsPicker.setMaxValue(99);

        counterView = view.findViewById(R.id.txtCounter);

        setCounterView(counterView);

        btnStart = view.findViewById(R.id.btnStart);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                if (redGradient.isRunning() || mediaPlayer!=null && mediaPlayer.isPlaying() ||
                        isTimerRunning())
                {
                    redGradient.cancel();
                    counterView.setTextColor(getResources().getColor(R.color.black54));

                    if (mediaPlayer!=null && initializedMediaPlayer)
                        mediaPlayer.stop();

                    stopCounting();
                    view.findViewById(R.id.pickersFrame).setVisibility(View.VISIBLE);
                    counterView.setVisibility(View.GONE);
                    btnStart.setText(R.string.label_count_down);
                }
                else
                {
                    view.findViewById(R.id.pickersFrame).setVisibility(View.INVISIBLE);
                    btnStart.setText(R.string.label_cancel);
                    counterView.setVisibility(View.VISIBLE);
                    //start count down
                    SubHourTime subHourTime = new SubHourTime();
                    subHourTime.setMinute(minutesPicker.getValue());
                    subHourTime.setSecond(secondsPicker.getValue());
                    subHourTime.setCentisecond(centiSecondsPicker.getValue());
                    restartCountDown(subHourTime);
                }
            }
        });

        return view;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (isTimerRunning())
        {
            getView().findViewById(R.id.pickersFrame).setVisibility(View.INVISIBLE);
            btnStart.setText(R.string.label_cancel);
            counterView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onTimerFinished() {
        redGradient.start();
        if (mediaPlayer!=null && initializedMediaPlayer)
            mediaPlayer.prepareAsync();
        btnStart.setText(R.string.label_dismiss_alarm);
    }
}
