package geek.bobby.chronometer.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import geek.bobby.chronometer.timing.ITimingContract;
import geek.bobby.chronometer.timing.PulseGenerator;
import geek.bobby.chronometer.timing.SimpleTimingPresenter;
import geek.bobby.chronometer.timing.SubHourTime;

/**
 * Contains base functionality for fragments containing a timer. This class holds a presenter and
 * a pulse generator. It creates them and cleans them up when destroyed. It class also saves and
 * restores current value of time counter.
 */
public abstract class BaseCounterFragment extends Fragment {
    private final ITimingContract.ITimePresenter presenter;
    private final PulseGenerator pulseGenerator;

    // These keys are used for saving and restoring the state
    private static final String KeyTimerWasRunning = "timerWasRunning";
    private static final String KeyLastMinuteValue = "lastMinuteValue";
    private static final String KeyLastSecondValue = "lastSecondValue";
    private static final String KeyLastCentiSecondValue = "lastCentiSecondValue";
    private static final String KeyLastSavingMoment = "lastSavingMoment";
    private static final String KeyCountingDirection = "countingDirection";

    public BaseCounterFragment() {
        presenter = new SimpleTimingPresenter();
        presenter.setOnFinishListener(new Runnable() {
                                          @Override
                                          public void run() {
                                              pulseGenerator.stop();
                                              onTimerFinished();
                                          }
                                      }
        );

        final int increment100thSecs = 10;

        pulseGenerator = new PulseGenerator(presenter, increment100thSecs);

    }

    /**
     * Override this method to get informed when timer reaches the last possible time
     */
    protected abstract void onTimerFinished();


    // Restarts the timer
    protected final void restartCounting()
    {
        presenter.reset();
        pulseGenerator.setDirection(PulseGenerator.Direction.CountUp);
        pulseGenerator.start();
    }

    // Stops the timer
    protected final void stopCounting()
    {
        pulseGenerator.stop();
    }

    // Restarts counting down
    protected final void restartCountDown(@NonNull SubHourTime fromTime)
    {
        presenter.reset();
        presenter.setCurrentTimerValue(fromTime);
        pulseGenerator.setDirection(PulseGenerator.Direction.CountDown);
        pulseGenerator.start();
    }

    protected final boolean isTimerRunning()
    {
        return pulseGenerator.isRunning();
    }

    // Always call this in onCreateView
    protected final void setCounterView(@NonNull ITimingContract.ICounterView view)
    {
        presenter.setView(view);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KeyTimerWasRunning, pulseGenerator.isRunning());
        if (pulseGenerator.isRunning()) {
            SubHourTime subHourTime = presenter.getCurrentTimerValue();
            outState.putLong(KeyLastSavingMoment, System.currentTimeMillis());
            outState.putInt(KeyLastMinuteValue, subHourTime.getMinute());
            outState.putInt(KeyLastSecondValue, subHourTime.getSecond());
            outState.putInt(KeyLastCentiSecondValue, subHourTime.getCentisecond());
            outState.putSerializable(KeyCountingDirection, pulseGenerator.getDirection());
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState==null)
            return;

        boolean wasRunning = savedInstanceState.getBoolean(KeyTimerWasRunning);
        if (wasRunning)
        {
            SubHourTime subHourTime = new SubHourTime();
            subHourTime.setMinute(savedInstanceState.getInt(KeyLastMinuteValue));
            subHourTime.setSecond(savedInstanceState.getInt(KeyLastSecondValue));
            subHourTime.setCentisecond(savedInstanceState.getInt(KeyLastCentiSecondValue));

            PulseGenerator.Direction direction = (PulseGenerator.Direction)savedInstanceState.getSerializable(KeyCountingDirection);

            // Time passed since the timer value was saved
            long passed = System.currentTimeMillis() - savedInstanceState.getLong(KeyLastSavingMoment);
            boolean canStartOver;
            if (direction== PulseGenerator.Direction.CountUp)
                canStartOver = subHourTime.incrementMillis(passed);
            else
                canStartOver = subHourTime.decrementMillis(passed);

            presenter.setCurrentTimerValue(subHourTime);
            // Possible for time to have been passed so much that the timer reaches its last
            // possible value
            if (canStartOver) {
                pulseGenerator.setDirection(direction);
                pulseGenerator.start();
            }
            else
                onTimerFinished();
        }
    }

    // Cleaning up components
    @Override
    public void onDestroy() {
        pulseGenerator.stop();
        presenter.onDestroy();
        super.onDestroy();
    }

    protected final SubHourTime getCurrentTimerValue()
    {
        return presenter.getCurrentTimerValue();
    }
}
