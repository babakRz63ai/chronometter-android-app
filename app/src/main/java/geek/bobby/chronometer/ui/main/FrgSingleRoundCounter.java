package geek.bobby.chronometer.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import geek.bobby.chronometer.R;

public class FrgSingleRoundCounter extends BaseCounterFragment
{

    @Override
    protected void onTimerFinished() {
        View view = getView();
        if (view==null)
            return;
        TextView btnStartPause = view.findViewById(R.id.btnStartPause);
        btnStartPause.setText(R.string.label_start);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_counter_single_round, container,
                false);

        final TextView btnStartPause = view.findViewById(R.id.btnStartPause);

        btnStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTimerRunning())
                {
                    stopCounting();
                    btnStartPause.setText(R.string.label_start);
                }
                else
                {
                    btnStartPause.setText(R.string.label_stop);
                    restartCounting();
                }
            }
        });

        setCounterView((CounterView)view.findViewById(R.id.txtCounter));

        return view;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        TextView btnStartPause = getView().findViewById(R.id.btnStartPause);
        if (isTimerRunning())
        {
            btnStartPause.setText(R.string.label_stop);
        }
        else
        {
            btnStartPause.setText(R.string.label_start);
        }
    }
}
