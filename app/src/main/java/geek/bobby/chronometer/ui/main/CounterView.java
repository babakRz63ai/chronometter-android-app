package geek.bobby.chronometer.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;

import java.util.Locale;

import geek.bobby.chronometer.timing.ITimingContract;
import geek.bobby.chronometer.timing.SubHourTime;

public class CounterView extends AppCompatTextView implements ITimingContract.ICounterView
{
    public CounterView(Context context) {
        super(context);
    }

    public CounterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CounterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void displayTime(@NonNull SubHourTime time)
    {
        setText(String.format(Locale.ROOT,"%02d:%02d.%02d",
                time.getMinute(), time.getSecond(), time.getCentisecond()));
    }

    @Override
    @NonNull
    public View getRootView() {
        return this;
    }
}
