package geek.bobby.chronometer.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import geek.bobby.chronometer.R;
import geek.bobby.chronometer.timing.SubHourTime;

public class FrgMultiRoundsCounter extends BaseCounterFragment
    implements View.OnClickListener
{
    private ViewGroup roundsItemsHolder;
    private int roundCounter = 1;
    private ScrollView scrollView;
    private TextView btnStart, btnStop;
    private ArrayList<SubHourTime> previousRounds = new ArrayList<>();

    private final String KeyPrevRounds = "FrgMultiRoundsCounter.PrevRounds";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_counter_multiple_rounds, container,
                false);
        btnStart = view.findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);
        btnStop = view.findViewById(R.id.btnStop);
        btnStop.setOnClickListener(this);

        roundsItemsHolder = view.findViewById(R.id.roundsItemsHolder);
        scrollView = view.findViewById(R.id.scrollView);

        CounterView counterView = view.findViewById(R.id.txtCounter);
        setCounterView(counterView);

        if (savedInstanceState!=null && savedInstanceState.containsKey(KeyPrevRounds))
        {
            Serializable serializable = savedInstanceState.getSerializable(KeyPrevRounds);
            if (serializable instanceof ArrayList) {
                previousRounds = (ArrayList<SubHourTime>) serializable;
                int counter = 1;
                for (SubHourTime subHourTime : previousRounds)
                    addViewItemForRound(subHourTime, counter++);
            }
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KeyPrevRounds, previousRounds);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
        if (isTimerRunning())
        {
            btnStart.setText(R.string.label_next_round);
            btnStop.setVisibility(View.VISIBLE);
        }
        else
        {
            btnStart.setText(R.string.label_restart);
            btnStop.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnStart:
                if (!isTimerRunning())
                {
                    // Restart operations
                    btnStart.setText(R.string.label_next_round);
                    btnStop.setVisibility(View.VISIBLE);
                    roundsItemsHolder.removeAllViews();
                    roundCounter=1;
                    previousRounds.clear();
                    restartCounting();
                }
                else
                {
                    previousRounds.add(getCurrentTimerValue());
                    addViewItemForThisRound();
                    roundCounter++;
                }
                break;

            case R.id.btnStop:
                stopCounting();
                previousRounds.add(getCurrentTimerValue());
                addViewItemForThisRound();
                btnStart.setText(R.string.label_restart);
                view.setVisibility(View.GONE);
                getView().findViewById(R.id.emptyFooter).setVisibility(View.GONE);
                break;
        }
    }

    private void addViewItemForThisRound()
    {
        addViewItemForRound(getCurrentTimerValue(), this.roundCounter);
    }

    private void addViewItemForRound(@NonNull SubHourTime time,
                                     int roundCounter)
    {
        final View item = LayoutInflater.from(getContext()).inflate(R.layout.item_timing_round,
                roundsItemsHolder, false);
        TextView index = item.findViewById(R.id.roundIndex);
        index.setText(String.format(Locale.getDefault(),"%d",roundCounter));
        TextView momentText = item.findViewById(R.id.roundTime);
        momentText.setText(String.format(Locale.ROOT,"%02d:%02d.%02d",
                time.getMinute(), time.getSecond(), time.getCentisecond()));
        roundsItemsHolder.addView(item);
        item.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollBy(0,item.getMeasuredHeight());
            }
        });
    }

    @Override
    protected void onTimerFinished() {
        if (getView()!=null) {
            addViewItemForThisRound();
            btnStart.setText(R.string.label_restart);
            btnStop.setVisibility(View.GONE);
        }
    }
}
